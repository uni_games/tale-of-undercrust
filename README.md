# The Tale of Undercrust - The Last 7 Days with Hasumi
> Version 1.2.5

## Greetings
Thank you very much for downloading "The Tale of Undercrust - The Last 7 Days with Hasumi".

If you have any comments or suggestions, please feel free to send them to the following e-mail address or blog.

Please enjoy the game.

## Overview
This is an RPG in which the player controls the main character, Hasumi, and defeats enemies while getting into slightly naughty situations.

This is an adult game, so those under 18 cannot play it.

The copyright of this software is owned by the creator, MIZUKU.

## Operation Method

| Key       | Action      |
|-----------|-------------|
| Cross key | Move the character |
| Z key     | Decide, advance message |
| X key     | Cancel, Start menu screen |
| Ctrl key  | Skip message |
| Shift key | Hide message window |


`Always dash` and `memorize last battle command` can be set from options.

In rare cases, you may not be able to select an enemy to attack during a battle.
In this case, you can solve the problem by selecting the enemy directly with the mouse.


## Material Usage

The following external materials were used in the production of this software :

### CG, tilesets, character chips

 - REFMAP : [http://refmap.wixsite.com/fsm-material/about](http://refmap.wixsite.com/fsm-material/about)

 - Bitter Sweet Entertainment Co., Ltd. 
   - (C) 2017 Bitter Sweet Entertainment Inc. [http://www.bitter-sweet.jp/](http://www.bitter-sweet.jp/)

 - Retiranatis Inc.

### Music and Sound Effects

 - Clown Possessed by the Moon : [http://uyuu.jp](http://uyuu.jp)

 - Bitter Sweet Entertainment Inc.
    - (C) 2017 Bitter Sweet Entertainment Inc. [http://www.bitter-sweet.jp/](http://www.bitter-sweet.jp/)
 - Ayato sound create : [https://ayatosoundcreate.wordpress.com/](https://ayatosoundcreate.wordpress.com/)

 - Hobby Studio Carrot Wynn : [http://carrotwine.muse.bindsite.jp/](http://carrotwine.muse.bindsite.jp/)
    - *Note: Link seems broken. I found this online :* [https://earthtear2149.wixsite.com/carrotwine](https://earthtear2149.wixsite.com/carrotwine), [https://soundcloud.com/carrotwine](https://soundcloud.com/carrotwine)

### Script

 - Triacontane (トリアコンタン) : [http://triacontane.blogspot.jp/](http://triacontane.blogspot.jp/)
 - Tomohei Akatsuki (赤月　智平) : [http://utakata-no-yume.net/](http://utakata-no-yume.net/)
 - Tomoaky : [http://hikimoki.sakura.ne.jp/](http://hikimoki.sakura.ne.jp/)
 - Yanfly Engine : [http://yanfly.moe](http://yanfly.moe)
 - Sasuke Kannazuki (神無月サスケ): [http://www.moonwhistle.org](http://www.moonwhistle.org)
 - Yoji Ojima

Some of the scripts I borrowed are distributed under the MIT license.
[http://opensource.org/licenses/mit-license.php](http://opensource.org/licenses/mit-license.php)


### Background Contribution

Rei Amatsuki (雨月レイ) [http://mimizuku0v0.wixsite.com/amatuki](http://mimizuku0v0.wixsite.com/amatuki)


### Game Testing

Merazu (めらず)

### Changelog

The current version is [Ver. 1.2.5].

#### Ver. 1.1.2 Fixes

 - Fixed a bug where the scene recollection was not released after defeating the boss of the second dungeon under certain conditions.
 - Fixed some mistakes in the included "Strategy Tips".
 - Fixed an issue where the door in the 5th dungeon would move to the wrong place.
 - Fixed a bug that caused the harness bind condition to recur under certain conditions.

#### Ver. 1.1.3 Fixes

 - Fixed an event that caused unnatural reactions in the 6th dungeon.
 - Fixed a bug where the store items were wrong after the 5th day.
 - Fixed a bug that caused the graphics of certain events to change depending on the direction you talk to the player.
 - Fixed a bug that allowed players to obtain dropped items from bosses in free battles as many times as they wanted.
 - Fixed a bug in the 5th dungeon that made it impossible to progress if you were defeated under certain conditions.
 - Fixed a bug that caused the harness condition to reappear after a free battle.
 - Fixed a bug that allowed you to enter the hot springs while harnessed.
 - Fixed an issue with the behavior of the boss in the 4th dungeon.
 - Fixed an issue with the release device in the 7th dungeon.
 - Fixed a bug in the 7th dungeon where using the release device would result in strange graphics.
 - Fixed an issue where the E-Shield's effect was not cut off even if the "Cut Effect" option was selected.
 - Fixed an issue where the conversation event with an enemy you've never seen before would occur again.
 - Fixed an issue where the E-Skill would remain a full attack even after removing the Diffusion Muzzle Unit.

#### Ver. 1.1.4 Fixes

 - Fixed an issue where enemies would behave strangely after being defeated in the 7th dungeon.
 - Fixed an error in the state resistance setting when using M Mode.
 - Fixed a bug in which resistance became impossible under certain conditions after the 4th dungeon boss 1 battle.
 - Fixed an issue where entering the fifth dungeon from the fifth base while in harness would allow you to proceed.
 - Fixed an issue where some events in the Recollection Room would not be released even after watching them.
 - Fixed an issue where the boss's picture book data in the second dungeon would not be registered.
 - Fixed an issue where some events in the reminiscence room would not be released after watching them.
 - Added the ability to release all review events after the ending.
 - Fixed a bug that prevented the event reminiscence from being released upon victory in Battle of Nikola 2.
 - Fixed a bug that caused the MP value to drop on the map when some drivers were equipped.
 - Fixed a bug that caused the standing image to shift when receiving enemy skills under certain conditions.
 - Added the ability to skip the OP movie.
 - Fixed a bug in the flag of the boss battle in the 5th dungeon.

#### Ver. 1.2.1 Fixes

 - Fixed a bug where bosses would not drop items under certain conditions.
 - Fixed a flag fix event that prevented the event reminiscence from being released in Nicola Battle 2.
 - Implemented a workaround for the error that occurs after defeating the mid-boss in the 5th dungeon.
 - Added the ability to return to the 7th day after the ending.
 - Implemented a driver that drastically weakens only attack power.
 - Added a debug lab in the reminiscence room (only after clearing the game).
   - {Infinite supply of items, ore explanation, junk chip replacement, and sexuality map generator}
 - Implemented an item that allows certain enemies to take over the harness binding and lidless state in free battles after the ending.

#### Ver. 1.2.2 Fixes
- Fixed a bug when using a debuff item in the reminiscence room.
- Fixed a bug where DOS-04 would use H-Skill right after DOS-04 was unbound.
- Fixed a bug where DOS-04 would not climax when MP was fully consumed by DOS-04's H-Skill.


####  Ver.1.2.3　Fixes
- Fixed typos and errors.
- Fixed a bug related to the resistance of some enemies to the H-Skill.
- Fixed a bug that caused some walls to be penetrated depending on the map tile settings.
- Fixed a bug in the 4th dungeon where the background music would stop when you turned around after entering the far end of the dungeon.
- Fixed a flag error in the event message display in the fifth dungeon.
- Fixed a bug that caused climax to be avoided when using the DOS-05 and -06 skills under certain conditions.
- Fixed a bug that caused the screen brightness to go wrong when using mail items under certain conditions.
- Fixed a bug in which the contamination value would become negative during review masturbation.


####  Ver.1.2.4　Fixes
 - Fixed a bug that allowed you to get items from certain enemies multiple times in free battles.
 - Fixed a bug that prevented saving under certain conditions.

####  Ver.1.2.5　Fixes
 - Fixed a bug that allowed players to enter the fourth dungeon even when in a double-sided state.
 - Fixed a bug that allowed you to enter the boss room of the 5th dungeon while harness bound under certain conditions.

## Acknowledgement

Orinial note : 

```
The Tale of Undercrust - The Last 7 Days with Hasumi

Produced and copyrighted by MIZUKU

Made with RPG Maker MV

Usage : Please execute "Game.exe" in the folder.

It is prohibited to distribute, reprint, upload, or sell this software without permission from the creator.
In no event shall the producer be liable for any damages arising from the use of this software.
Please use this software at your own risk. 
There is no relationship between the person, organization, or any other unique name appearing in this software and the real thing.
Please note that the game may not start depending on the performance and environment of the computer you are using.
```

Please have a valid copy of the game before anything. I'm hosting translations and scripts, not the full game.

From a legal point of view, you'd need the game to use this. 

## Getting creative
Well, modding RPG Maker MV game ain't hard. For this, I recommend some JavaScript skills and python scripts for script-related operations.
If you've found this, I guess you're qualified enough to install python and use this, isn't it ? ;) 

The basic starting point is to serve the `www` folder though a webserver. Python do this great !

```
cd www
python -m http.server 5000
```

Then simply pop your browser to [http://127.0.0.1:5000/](http://127.0.0.1:5000/)

### Change Save data

*Note: This part is a bit technical, I'm not gonna explain in details*

With the devtools, change the local storage values. All are Base64 lz-compressed JSON, that can be modified using some python scripts (scripts/saves.py)
 - `RPG Globa` : Global game state
 - `RPG Config` : Options
 - `RPG File1` : First save
 - `RPG File2` : Second save

### Translate anything

I'd recommand using bith [Deepl](https://www.deepl.com/en/translator) and [Google Translate](https://translate.google.com/?sl=ja&tl=en&op=translate) for any translations.

Google Translate performs better with highly technical word without context, where Deepl is the best for dialogs.

Messages are in the huges JSON files located in "www/data/". It is recommanded to use Notepad++ or some editor that handles big files.

I personally use PyCharm with boosted indexing so I can quickly do some global word search and name replacement.

[JSONEditorOnline.org](https://jsoneditoronline.org/) can help you beatifying huge JSON files.

You can send pull request of modified translations. 

### Play on mobile

Okay you theorically *can* do that, simply serve the files using a webserver and connect your phone to it. I'd recommand a browser that support fullscreen.

I personally did this using termux with python and having the whole www folder (~500mb) on my sdcard.

If someone's brave enough to host it, please PR with the link to it.

I've added a side button "Esc" for simulating keyboard on mobile.