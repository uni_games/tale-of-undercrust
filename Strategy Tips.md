
# Strategy Tips

 1. When you are stumped in exploring a scenario or dungeon
 2. When you have a hard time in battle
 3. When you want to lose in battle
 4. When you find a bug.
 5. When you want to say a few words to the creator
 6. Hints for finding hidden dungeons
 7. Hints for finding items to review.



## When you are stumped in exploring a scenario or dungeon

If you are unsure of how to proceed, check your item's "Notepad".
It will update what you have to do now, and will help you explore.
This will help you in your exploration.


## When you have a hard time in battle

Even seemingly strong enemies have their weaknesses.
When you have a hard time in battle, check the item "Memo Pad" or "Exoplanet Book".
You may be able to get some good information.

Also, Hasumi's abilities vary greatly depending on the combination of drivers.
If you find it difficult to deal damage, you may want to review your equipment.

As for attributes, I'll give you a rough idea.
Slimy enemies and plants are vulnerable to fire and ice attributes, people are vulnerable to lightning, and machines are vulnerable to plasma.
If you really want to defeat an enemy, switch out the treasure ball and try fighting it.



## When you want to lose in battle

If you want to lose in battle, but you are much stronger than the enemy, you can use the following
You can make it easier to lose by equipping a debuff driver.

Anti-Immune (obtained from the Second Laboratory)
You can disable the restraint state resistance and hypnotic state resistance when transforming.

Energy Limiter 50 (obtained from the First Research Institute)
Reduces all of Hasumi's base ability values by half.

KERS Canceller (obtained in the Recollection Room)
Cancels the recovery of MP by E-Sharge and the recovery of MP after the end of battle.
Please use this when your MP does not decrease easily.



## When you find a bug

You can send pull request, and we'll take a look at them. Note that we are very busy and may be very long to respond...

<details>
  <summary>Original content, not relevent anymore.</summary>

If you have any bug reports, please send them to the blog :
  [http://mizukunokimi.blog.fc2.com](http://mizukunokimi.blog.fc2.com).

Please note that it may take some time to respond depending on the contents of the bug and the busy status of the circle.
We will do our best to respond to bugs as soon as possible.

</details>




## If you want to say something to the creator

Check mizukunokimi's blog and buy its games. I'm putting this definitely unofficially, so please don't spread the word about this repo.

<details>
  <summary>Original content, if you want to support (And you'd better do >:3 ).</summary>

If you have any comments on the game, please feel free to comment on the blog of Circle Octagon Family.
http://mizukunokimi.blog.fc2.com

If you want to send an e-mail, please send it to this address :
`mizuku577@yahoo.co.jp`

We are also conducting a survey about the product.
If you have time, please help us.

</details>

# Spoilers for the next part of the game.


<details>
  <summary>This may spoil the game ! Use with care ...</summary>

## Hints for finding the hidden dungeon

### 6-1 (from a decaying abandoned mine)
At the end of the passage where the gas is protruding, there may be a path that has not yet been visited by people.
It's a little dangerous, but try to burn through the gas once.




### 6-2 (From the cold air side hole)
The rock wall may be brittle in the steep rocky area.
Tap here and there to see what happens.
If the underground water drains out, the room where the water was collected on the upper floor can be found at ......?



### 6-3 (from Karami Marsh)
There is a rumor that the hole where the parasite lives leads to somewhere else.
Once you've killed the worms that sprout up, you'll be able to continue on to the next level.
Also, if you get a new treasure in the adventure ahead, you may be able to cut off the green rock with ivy on it and move on.
You may be able to proceed to the next level.



### 6-4 (from the first laboratory)
At first glance, the entrance to the building looks empty, but there seems to be a secret in the rocky area to the left of it.
Please examine the cracks carefully.



### 6-5 (from Hoshangiden)
The Hoshangiden has been modified in various ways by the cult members, but the rocky area at the entrance has not been investigated yet.
However, the rocky area at the entrance seems to have not been investigated yet.
If you examine the cracks while watching out for the eyes of the guards, you will find ......?



## 7. Hints for finding items to review

### 7-1 Power of Water Pressure
If you drill a hole in a pressurized layer of groundwater.
If you drill a hole in a place where groundwater is collected in a pressurized layer, even the hardest bedrock will be blown away.
You may find something unusual hidden in the hole.



### 7-2 [Celestial Chart]
This drawing found in an underground temple may not be a star map, but a map.
This drawing found in the underground temple may not be a star map but a map.
The hint is the map where Nikola was.
The hint is somewhere in the map where Nikola was.



### 7-3 [Ivy Curtain]
It's easy to overlook in the dark and narrow passages, but behind the rock walls hidden by ivy
Sometimes there are hidden paths behind rock walls hidden by ivy.
Change to S-mode and try to cut the suspicious ivy.



### 7-4 [Prophecy Observer]
The Prophecy Observer cannot be destroyed from the outside.
In the small room to the right of the stairs on the fourth floor, find a note with a mathematical formula written on it.
The numbers A to D apply to the numbers you enter in order from left to right.
If you can cause a calculation error by entering some numbers, you may be able to break the observation machine.
If you can generate a calculation error by entering some numbers, you may be able to make the observation machine fail.

Have you ever encountered an error when calculating with a calculator?
Have you ever heard of a number that should not be placed in the denominator of a fraction?
There is one calculation that machines are very bad at.
There is only one calculation that machines are not very good at, and that is the number of fractions.



### 7-5 [Collapsed hallway]
The hole in the floor of the collapsed corridor on the second floor is too big to jump over.
Find a route around it.
There should be a path hidden by a curtain at the back of the floor.

### [Pluto]
In the basement of this mansion, there seems to be a secret room that was buried by the master.
Let's examine the stone walls.



### 7-6 [Suspicious Research]
Some researchers of the `Eye of Aquarius` are developing technology to crack the neural circuits of androids.

Examine the large laboratory on the first basement floor where the brainwashers roam. Take a closer look.

</details>