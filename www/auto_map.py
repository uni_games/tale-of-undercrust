n = 25

f = ("{:>4}, " * n)


d = data

dd = [d[i:i+n] for i in range(0, len(d), n)]

for l in dd:
   print(f.format(*l))


   """
   "parameters": [
                0,
                1,  /* id map */
                1,  /* x */
                1,  /* y */
                0,
                0
              ]
   """