// ========================================================

console.log(("Intruded !"))

console.debug("Setup plugins ...")
console.debug($plugins)

// Setup hacks
window._h4k = {};

// Damages will be at 0
window._h4k.noDmg = false;


function callUI() {
    window._h4k.noDmg = !window._h4k.noDmg;
    alert(`UI Called !
    inBattle: ${$gameParty.inBattle()}
    noDMG: ${window._h4k.noDmg}
    `);

    }

PluginManager.setup($plugins);
window.onload = function() {
    console.log("window.onload");

    // Patching respondive
    Graphics._defaultStretchMode = function() {
        return true;
    };

    // Create UI CSS
    var css = '.btn { \
        border: 1px solid #7f45e7; \
        box-shadow: inset 0 0 100px #54a7ff66, 0 0 30px rgb(0 0 0 / 20%); \
        color: white; \
        background-color: #2b037a; \
        position:absolute; \
        font-family: monospace; \
        font-size: 16px; \
        text-align: center; \
        padding: 4px; \
        width:16px; height:16px; \
        opacity:0.8; z-index:100; \
    } \
    \
    .dbg_btn {\
        right:8px; top:8px; \
    }\
    \
    .esc_btn {\
        right:8px; top:48px; \
        padding: 8px; \
    }\
    .shift_btn {\
        right:8px; top:96px; \
        padding: 8px; \
    }\
    ';
    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement('style');

    head.appendChild(style);

    style.type = 'text/css';
    if (style.styleSheet){
    // This is required for IE8 and below.
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }

    var elem = document.createElement('div');
    elem.className = 'btn dbg_btn';
    elem.innerHTML = '»';
    elem.onclick = function(){
        callUI();
    };

    document.body.appendChild(elem);

    var esc = document.createElement('div');
    esc.className = 'btn esc_btn';
    esc.innerHTML = 'Esc';
    esc.onclick = function() {
        console.log('esc');
        Input._onKeyDown(new KeyboardEvent('keydown',{'keyCode':27}));
        setTimeout(() => {  Input._onKeyUp(new KeyboardEvent('keyup',{'keyCode':27})); }, 500);

    };
    document.body.appendChild(esc);

    // var shift = document.createElement('div');
    // shift.className = 'btn shift_btn';
    // shift.innerHTML = 'Shf';
    // shift.onclick = function() {
    //     console.log('shift');
    //     Input._onKeyDown(new KeyboardEvent('keydown',{'keyCode':16}));
    //     setTimeout(() => {  Input._onKeyUp(new KeyboardEvent('keyup',{'keyCode':16})); }, 100);
    //
    // };
    // document.body.appendChild(shift);
    
    try {
        SceneManager.initialize();
        SceneManager.goto(Scene_Boot);
        SceneManager.requestUpdate();
    } catch (e) {
        SceneManager.catchException(e);
    }

    // SceneManager.run(Scene_Boot);

};
